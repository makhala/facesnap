import { Component, Input, OnInit } from "@angular/core";
import { FaceSnap } from "../models/face-snap.model";
import { FaceSnapsService } from "../services/face-snaps.service";

@Component({
  selector: "app-face-snap",
  templateUrl: "./face-snap.component.html",
  styleUrls: ["./face-snap.component.scss"],
})
export class FaceSnapComponent implements OnInit {

constructor(private faceSnapsService: FaceSnapsService){}
@Input() faceSnap:FaceSnap;
  textLikeButton: string;

  ngOnInit() {
    this.textLikeButton = "Like";
  }
  onLikeOrDislike() {
    if (this.textLikeButton == "Like") {
    this.faceSnapsService.snapFaceSnapOrUnSnapById(this.faceSnap.id,'like');
    this.textLikeButton = "Dislike"
    }else{
    this.faceSnapsService.snapFaceSnapOrUnSnapById(this.faceSnap.id,'dislike');
    this.textLikeButton = "Like"
}
  }
}
