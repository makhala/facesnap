import { Injectable } from "@angular/core";
import { FaceSnap } from "../models/face-snap.model";
import { forEach } from "@angular/router/src/utils/collection";

@Injectable({
  providedIn: "root",
})
export class FaceSnapsService {
  faceSnaps: FaceSnap[] = [
    {
      id: 1,
      title: "Le lion de Sénégal",
      description: "Ceci est un lion",
      imageUrl:
        "https://images.unsplash.com/photo-1614027164847-1b28cfe1df60?w=500&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8Mnx8bGlvbnxlbnwwfHwwfHx8MA%3D%3D",
      createdDate: new Date(),
      snaps: 250,
      location: "Herblay",
    },
    {
      id: 2,
      title: "Tigre",
      description: "Ceci est un tigre",
      imageUrl:
        "https://images.unsplash.com/photo-1605092676920-8ac5ae40c7c8?w=500&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8M3x8dGlncmV8ZW58MHx8MHx8fDA%3D",
      createdDate: new Date(),
      snaps: 1,
      location: "Paris",
    },
    {
      id: 3,
      title: "Elephant",
      description: "Ceci est un éléphant",
      imageUrl:
        "https://images.unsplash.com/photo-1603483080228-04f2313d9f10?w=500&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8M3x8ZWxlcGhhbnR8ZW58MHx8MHx8fDA%3D",
      createdDate: new Date(),
      snaps: 120,
    },
    {
      id: 4,
      title: "Le lion de Sénégal",
      description: "Ceci est un lion",
      imageUrl:
        "https://images.unsplash.com/photo-1614027164847-1b28cfe1df60?w=500&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8Mnx8bGlvbnxlbnwwfHwwfHx8MA%3D%3D",
      createdDate: new Date(),
      snaps: 250,
      location: "Herblay",
    },
    {
      id: 5,
      title: "Tigre",
      description: "Ceci est un tigre",
      imageUrl:
        "https://images.unsplash.com/photo-1605092676920-8ac5ae40c7c8?w=500&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8M3x8dGlncmV8ZW58MHx8MHx8fDA%3D",
      createdDate: new Date(),
      snaps: 1,
      location: "Paris",
    },
    {
      id: 6,
      title: "Elephant",
      description: "Ceci est un éléphant",
      imageUrl:
        "https://images.unsplash.com/photo-1603483080228-04f2313d9f10?w=500&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8M3x8ZWxlcGhhbnR8ZW58MHx8MHx8fDA%3D",
      createdDate: new Date(),
      snaps: 120,
    },
  ];
  getAllFaceSnaps(): FaceSnap[] {
    return this.faceSnaps;
  }
  getSnapById(faceSnapId: number) {
    const faceSnap = this.faceSnaps.find(
      (faceSnap) => faceSnap.id === faceSnapId
    );
    if (faceSnap) {
      return faceSnap;
    } else {
      throw new Error("FaceSnap not found!");
    }
  }
  snapFaceSnapOrUnSnapById(faceSnapId: number, snapType: 'like'|'dislike'): void {
    const faceSnap = this.faceSnaps.find(
      (faceSnap) => faceSnap.id === faceSnapId
    );
    snapType==='like' ? faceSnap.snaps++ : faceSnap.snaps--;

  }
}
