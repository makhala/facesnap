import { NgModule } from "@angular/core";
import { FaceSnapListComponent } from "./face-snap-list/face-snap-list.component";
import { RouterModule, Routes } from "@angular/router";
import { PageAccueilComponent } from "./page-accueil/page-accueil.component";

const routes: Routes = [
  { path: "facesnaps", component: FaceSnapListComponent },
  { path: "", component: PageAccueilComponent },
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
